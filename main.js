import App from './App'

// #ifndef VUE3
import Vue from 'vue'

Vue.config.productionTip = false
App.mpType = 'app'

import store from '@/store';

// 引入 uView 的 js 库
import uView from "uview-ui";
Vue.use(uView);

import '.env.js'

let vuexStore = require("@/store/$u.mixin.js");
Vue.mixin(vuexStore);

import chat from "@/static/js/chat/chat"
uni.$u.chat = chat;

import api from "@/config/api";
uni.$u.http.api = api;

// let vuexStore = require("@/store/$u.mixin.js");
// Vue.mixin(vuexStore);

try {
    function isPromise(obj) {
        return (
            !!obj &&
            (typeof obj === "object" || typeof obj === "function") &&
            typeof obj.then === "function"
        );
    }

    // 统一 vue2 API Promise 化返回格式与 vue3 保持一致
    uni.addInterceptor({
        returnValue(res) {
            if (!isPromise(res)) {
                return res;
            }
            return new Promise((resolve, reject) => {
                res.then((res) => {
                    if (res[0]) {
                        reject(res[0]);
                    } else {
                        resolve(res[1]);
                    }
                });
            });
        },
    });
} catch (error) {
}

const app = new Vue({
    store,
    ...App
})

// 引入请求封装，将app参数传递到配置中
require('@/config/request.js')(app)

app.$mount()
// #endif

// #ifdef VUE3
import {createSSRApp} from 'vue'

export function createApp() {
    const app = createSSRApp(App)
    return {
        app
    }
}

// #endif