let noFullScreenTheme = {
    "--container-max-width": "1200px",
    "--container-border-radius": "20px",
    "--window-width": "90vw",
    "--window-height": "90vh",
};
export default {
    theme: {
        "--theme": "light",
        "--white": "#fff",
        "--black": "#303030",
        "--gray": "#fafafa",
        "--primary": "#1d93ab",
        "--second": "#e7f8ff",
        "--hover-color": "#f3f3f3",
        "--bar-color": "rgba(0, 0, 0, .1)",
        "--theme-color": "$gray",
        "--shadow": "50px 50px 100px 10px rgba(0, 0, 0, .1)",
        "--card-shadow": "0px 2px 4px 0px rgba(0, 0, 0, .05)",
        "--border-in-light": "1px solid #dedede",
        "--sidebar-width": "300px",
        "--window-content-width": "calc(100% - var(--sidebar-width))",
        "--message-max-width": "80%",
        "--full-height": "100vh",
        "--container-min-width": "600px",
        "--container-max-width": noFullScreenTheme["--container-max-width"],
        "--window-width": noFullScreenTheme["--window-width"],
        "--window-height": noFullScreenTheme["--window-height"],
    },
    noFullScreenTheme,
    fullScreenTheme: {
        "--container-max-width": "unset",
        "--container-border-radius": "unset",
        "--window-width": "100%",
        "--window-height": "100vh",
    }
}