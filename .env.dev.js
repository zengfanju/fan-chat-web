const config = {
    // #ifdef H5
    baseUrl: "/api/",
    // #endif
    // #ifndef H5
    baseUrl: "http://127.0.0.1:8888/",
    // #endif
}
module.exports = config;