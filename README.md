# FanStars ChatGPT

- FanStars 是基于 [ChatGPT-Next-Web](https://github.com/Yidadaa/ChatGPT-Next-Web/tree/b1f27aaf93c88c088db6bae5ac8163e2ffe991bd) 的css样式开发的 ChatGPT 网页应用，觉得不错请右上角Star。
- 项目前端采用[uni-app](https://uniapp.dcloud.net.cn/)、[uview-ui](https://www.uviewui.com/)进行开发。
  - [gitee地址](https://gitee.com/zengfanju/fan-chat-web)
- 项目后端采用Java进行开发。
- [体验地址](http://chat.zengfj.cn)

## 本地运行

1. 因本项目是基于[HBuilder X](https://www.dcloud.io/hbuilderx.html)进行开发，所以要下载[HBuilder X](https://www.dcloud.io/hbuilderx.html)客户端。

2. 需要[nodeJs](https://nodejs.org/en/download/)环境，12以后的版本。

   ```shell
   # 安装完node之后，安装项目必要的依赖，在项目目录下运行
   npm install
   
   # 因node版本不一样，可能package-lock.json配置也不一样，
   # 所以会安装失败，因此执行执行下面的命令
   npm install uview-ui markdown-it highlight.js uuid --save
   ```

3. 打开[HBuilder X](https://www.dcloud.io/hbuilderx.html)客户端，导入项目。

4. 目前后端还在完善中，还没开源，可以先使用我的后端，在manifest.json中修改h5的配置。

   ```json
       "h5" : {
           "devServer" : {
               // 开发环境
               "proxy" : {
                   // 代理转发
                   // 将你的接口最后公共部分提取出来
                   "/api" : {
                       "target" : "http://chat.zengfj.cn/api/",
                       // 以/api 开头的代理到 target指定地址
                       "pathRewrite" : {
                           "^/api" : ""
                       }
                   }
               }
           }
       }
   ```

5. 在[HBuilder X](https://www.dcloud.io/hbuilderx.html)客户端中运行项目到谷歌。

6. 有什么不会的，可以问[chat-gpt](http://chat.zengfj.cn)

