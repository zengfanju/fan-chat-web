import { sendMessage } from "@/config/sse.js"

const http = uni.$u.http;

let baseUrl = http.config.baseURL;
if (baseUrl && baseUrl.endsWith("/")) {
    baseUrl = baseUrl.substring(0, baseUrl.length - 1);
}

const catchTrue = {custom: {catch: true}};

const chatPrefix = "/chat";
const sseUrl = baseUrl + chatPrefix + "/sse";
const context = (params, config = catchTrue) => http.post(chatPrefix + "/context", params, config);
const summaryTopic = (params, config = {}) => http.post(chatPrefix + "/summary/topic", params, config);

export default {
    sseUrl,
    context,
    summaryTopic,
    sendMessage
}